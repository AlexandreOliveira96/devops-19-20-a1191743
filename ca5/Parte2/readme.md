# Class Assignment 5

## Part 1

#### Implementation
To perform this tutorial, you will need to download Jenkins war file and our repository, with
the basic directories and programs inside. To get started let's just create a new directory
in our repo called ca5/parte1, where we will be uploading our files.

First, we should change directory in a PowerShell (with admin privileges) to the place
where we have our jenkins.war file. Here we will just run it with `java -jar jenkins.war`.

This will trigger an event causing localhost:8080 to display a Jenkins page, with a Login page.
Your first password will appear in your terminal, after that you just need to configure your jenkins
account to get started.

We will then create a new job in Jenkins, that will need a pipeline that we will write in our
jenkinsfile. We need to setup the job so that it runs into our desired repository
and folders where we need it to perform. After that we just need to create the jenkinsfile and tell
Jenkins to build it.

I used windows commands, send Jenkins my Git credentials and created four stages with tasks inside.
As we are going to use gradle commands, we just need to tell jenkins to go into our directory containing
gradle and everything we will need for it to work out, like this.

For the Checkout stage:
```
stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: '1191743-bitbucket', url: 'https://bitbucket.org/AlexandreOliveira96/devops-19-20-a1191743/'
            }
        }
```
Now for the Assemble:
```
stage('Assemble') {
            steps {
                dir('ca2/Parte1') {
                    echo 'Building...'
                    bat './gradlew clean assemble '
                }
            }
        }
```
For the Test:
```
stage('Test') {
            steps {
                dir('ca2/Parte1') {
                   echo 'Testing...'
                   bat './gradlew test'
                   junit 'build/test-results/test/*.xml'
                }
            }
        }
```
And finally, for the archiving stage:
```
stage('Archiving') {
            steps {
                dir('ca2/Parte1') {
                bat 'dir'
                echo 'Archiving...'
                archiveArtifacts 'build/distributions/'
            }
            }
        }
```

After getting our jenkinsfile just as it should be,
we will push it to our Bitbucket repository and then, Build it. If everything works out, your jenkins page
will look something like this.

![](resources/imagem1.png)

Clicking on the `#16` (on the left) on this case, it's the build number. Here you can see a short message telling 
you what the build did, when he did it, files uploaded, test results, and so on.
Here you have an option to look further into it. This is what you will probably see:

![](resources/imagem2.png) 

Now, lets go to the second part of the assignment.

## Part 2

#### Implementation

For the second part, the setup is exactly the same, create a directory called ca5/parte2, 
run and sign into jenkins from your terminal, setup your new job and create a jenkinsfile 
and start editing it. 

So, here we will need some new Stages, such as javadoc, for starters. To perform this we 
will need to download a Jenkins plugin, called HTML Publish, otherwise you won't be able to
do it. After this, we will add the following command on the javadoc stage:
```
publishHTML ([
              reportName: 'Javadoc',
              reportDir: 'build/docs/javadoc/',
              reportFiles: 'index.html',
              keepAll: false,
              alwaysLinkToLastBuild: false,
              allowMissing: false
             ])
```

Next, let's get our Docker stages going. The rest of the jenkinsfile is the same as in ca5-parte1
so I won't be getting too much into that. At this moment I created a new repository on DockerHub 
called ca5 and then just opened up a new terminal to make docker run on my computer.

I told jenkins to build an image of my project and gave it my profile's url, in my publish image stage, 
then created a new stage to publish this image. I need to give my credentials to jenkinsfile and tell 
it to push this image to our repository.
```
stage('Docker Image') {
                     steps {
                     dir('YourDirectoryHere') {
                            script {
                            dockerimage = docker.build("YourDockerLoginHere/JobNameHere:${env.BUILD_ID}")
                            }
                        }
                    }
                }
```
(and now let's publish it)
```
stage('Publish Image') {
                    steps {
                        dir('YourDirectoryHere') {
                            script{
                                bat 'docker login -u="YourDockerLoginHere" -p="YourDockerPasswordHere"'
                                dockerimage.push()
                            }
                        }
                    }

        }
```

Now we just need to push this edits into our bitbucket repo and tell jenkins to fetch it and build it.
If everything went well, you will see pretty much this:
![](resources/imagem3.png) 

By going into our Docker-Hub repository, we can make sure that the image was pushed with
the tag we told it to do so. Again, if everything worked out properly, you will have that
image published on your repo, as so:
![](resources/imagem4.png) 

Now, after performing the two builds, for ca5 part1 and part2, this is what you should be seing
in your Jenkins homepage, and if so, it all worked out properly:

![](resources/imagem5.png)

And that's it, for the Class Assignment 5.

## Jenkins Analysis

Jenkins is a really useful tool for projects, very popular among projects and it's even said that
if jenkins can't do it, then no one can. I found that it works really well, it's easy to setup and
it works just like expected. It can do pretty much everything and saves developers a lot of work.

## Alternative - with Buddy

#### Alternative Implementation

For an alternative to Jenkins I will be using [Buddy](https://app.buddy.works) software,
a very simple and nice to look at alternative to jenkins. 

First you will just need to create an account, sign in with your bitbucket account so that it
has access to it, and just create some actions (same as Jenkins stages), and then make it build
on your last commit. You will be able to view the build as it is being made
as well as fix your errors without getting several unbuild icons on your bitbucket.

Now let's create some actions, first one is Assemble:
```
cd ca2/Parte2
echo "Building..."
gradle clean assemble 
```
For the action Test:
```
cd ca2/Parte2   
echo 'Testing...'
gradle test
```
For generating Javadoc:
```
cd ca2/Parte2
echo 'Generating javadocs...'
gradle javadoc
```
To build the docker image, we can choose this one action from their useful list,
and then simply tell Buddy the way to this file:
```
ca5/Parte2/Dockerfile
```
For the action to push the image we need to do the same process as before and type 
some information. Let's tell Buddy to use the image built in the
previous action. Then we need to inform that we are submitting this to Docker Hub,
and give our login information. Then, we will say which Docker Repository to use,
and the name of the tag. I used `$BUDDY_EXECUTION_ID` for the tag, so that it gets 
the name of the number of the build. This is what you should have, more or less:

![](resources/imagem6.png)

Now that we have all our actions ready and up-to-date, we can now tell Buddy to run this 
pipeline. It should look like this:
 
![](resources/imagem7.png) 

When you click on `Run pipeline`, Buddy will ask your for a commit where 
you want this build to be executed, I choose the last one and clicked on `Run now`.
Now, if you run into some error, Buddy will let you edit your command and retry 
from the spot it found that error, allowing you to quickly fix what you need 
and keep going. This is really useful. If all goes as it should, your last commit
on Bitbucket will have a check mark, like this:

![](resources/imagem8.png) 

And there you go, the build is successfully done and everything worked out great. Again, 
we will just check our Docker-Hub repository and make sure the image was posted there.

![](resources/imagem9.png) 

After this, we can export our [pipeline.yaml](resources/devops-19-20-a1191743-pipelinesBuddy.yml) file
so that we can share with or import from some other person. This is very useful and helpful.

#### Alternative Analysis

After using Jenkins and Buddy, I really liked Buddy. Even though it can't probably do
everything that jenkins does, I found it extremely easy to understand and much more user 
friendly. 

As opposed to jenkins, you don't need to pre-install anything, as everything works 
on your browser, neither do you need to install plugins. You don't need to run docker 
as well in order for everything to work. This, in my case, was amazing because it spared 
a lot of work for my computer. 

The way you make the pipelines in Buddy it's really nice, you just choose the technology 
you'll want to use and it pre-sets your script, so you need to do less extra work. Then, 
as I said before, if you run into an error, you can just edit your script and it will retry
where you left off, just amazing and time saver. This, as opposed to getting your builds in 
Jenkins to go up and up just because some little error is another advantage for Buddy.

Overall I preferred Buddy, even though I admit that in some ways, Jenkins would be more useful,
but *user friendliness* is very important, and Buddy gets that point. 

 