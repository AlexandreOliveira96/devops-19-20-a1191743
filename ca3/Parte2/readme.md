# Class Assignment 03 Part 2

### Prerequisites
To perform this tutorial you will need to [install VirtualBox](https://www.virtualbox.org/) 
We need a new directory in our repository called ca3/parte2 and upload the vagrantfile 
in the professor's repository into our own.

Because we were going to use or Ca2-parte2 files, to run a gradle project, I made a copy of it so
that I could adapt it to go smoothly on vagrant and everything could work out.


### Vagrantfile update
We need to update the vagrantfile so that it can clone my repository.
```
git clone https://bitbucket.org/AlexandreOliveira96/devops-19-20-a1191743.git
```
Then, we can update the change directory command as well.
```
cd devops-19-20-a1191743/ca3/Parte2/gradle
```
Because the VM couldn't get to use the gradle package I had in my repository,
I had to add a change mod command.
```
chmod +x gradlew
```
Mostly all of the commands had to be with `sudo` before it, so that it could work on files
with different permissions.

After all of this little changes on the vagrantfile, I will update my gradle package, 
according to the professor's repository. So, I updated my `application.properties`,
my `app.js` and the `build.gradle` files.

### Vagrant
Then, on a terminal, you can just change to your repo directory and do:
```
vagrant up
```
If everything works out, you just need to wait some minutes and you can open your
virtual Box and check that now you have two new VM, one called web and another called db, as
we prepared in the vagrantfile.

I encountered some problems loading up the browser, because in some places I had 
`demo-0.0.1-SNAPSHOT.war` and other places it was called
`basic-0.0.1-SNAPSHOT.war`. So, I just replaced all with demo, discarded my VMs 
and retried to run vagrant up.


### On the frontend
This time all went great, I opened the browser in:
```
http://localhost:8080/demo-0.0.1-SNAPSHOT/
```
And now I could see the page loaded, with the fields for 
the employees and one employee as well.

After this, I went to check the H2 database, using:
```
http://localhost:8080/demo-0.0.1-SNAPSHOT/h2-console
```
I could see a login for the H2 db and used `jdbc:h2:tcp://192.168.33.11:9092/./jpadb`
to connect with it. Here I did some basic tasks like view the table, insert new Employees, delete entries, and so on.
I noticed that, if I updated the Database, then the browser couldn't show me the new data.
I don't know if this was something I've done wrongly, but, by using
```
vagrant reload
```
The data would reload on the VM and I could it see it on the browser again.


## Vagrant Analysis

Vagrant seems to be a very good option when dealing with multiple virtual machines, creating
and sharing VMs with anyone(and anywhere). It has a simple mechanism that just works. It's very easy
to understand and implement as a non experienced user. Its vagrantfile is fairly understandable 
and easy to replicate. Even though creating Virtual Machines from scratch tends to be time consuming,
with vagrant it can do the job in 10 minutes or so. Sometimes it may malfunction depending
on permissions and or user settings, such as having a user name with some special character,
but overall I believe it's a really useful tool and easy to understand.


## Alternative

As I was looking for alternatives to VirtualBox, I came across some different choices, some were even more
popular than VirtualBox and some offer different features, but I focused more on Hyper-V and VMware
and why I coulnd't work with them. 

I studied both for a bit and learned that `Hyper-V` only work properly on Windows 10 Enterprise, Pro,
and Education, which is a great disadvantage for using VirtualBox.

After checking on `VMware` I realized that you either buy the Pro Version, or you 
can get the Free or the Pro Trial one, that only lasts a month. 
The drawback here is that this Free or the Pro Trial don't have official Vagrant support, so It 
won't run on your VM. The Free only lets you run 1 VM at a time, so this class assignment wouldn't be
possible there. Also, VMware only functions with computers x64.

