# Class Assignment 03 Part 1 

## Prerequisites
To perform this tutorial you will need to [install VirtualBox](https://www.virtualbox.org/) 
Then you need to install all the prerequisites of the CAs whe used before (git, jdk, maven and gradle)
like this:
```shell script
sudo apt install git
```
At last, we can clone our repository to the VM using `git clone`.

## Analysis, Design, and Implementation
If you prefer to work on your desired command line, you can use the VM and ssh to connect with 
your VM from another place. After ssh setup you can open another terminal and simple do:

```sh
$ssh yourVMUserName@your.VM.'s.ip
```

### Build Spring Boot Tutorial with Maven

Change your current directory to the basic folder of ca1 and build the project:

```sh
$mvn build
```
Execute the application using the following command:
```sh
$mvn spring-boot:run
```
According to the application started running, now you can access the web applications from the browser in your host machine:

```
your.VM.'s.IP:8080
```


### Build Gradle_basic_demo With Gradle

Now let's change to the basic folder of ca2-Parte1 and build the project:
```sh
$gradle build
```
On this step I ran into trouble. My gradle's version was not the required to build this project.
So, I had to install the updated version using a packet manager called `sdkman` 
following the [instructions](https://docs.gradle.org/current/userguide/installation.html), like so:

```sh
sudo apt install curl
sudo apt install zip
curl -s "https://get.sdkman.io/" | bash
source "$HOME/.sdkman/bin/sdkman-init.sh"
sdk install gradle
```

Finally, build the gradle_basic_demo:

```sh
$gradle build
```

You can now run some tasks and confirme that everything works as predicted.

### Run the server

Run Server task in your Virtual Machine:

```sh
$gradle runServer
```
Then, open another terminal in your host machine to test the runClient task.
This will not turn out great on your VM since it does not have a graphical interface, and an error will appear.
One way to overcome this, is by executing, without needing to change your project code, the jar file from the root project directory from your host machine.
```
$java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatClientApp your.VM.'s.ip 59001
```
If done properly, now should open up a new window with the desired Chatter.

To run several clients, you just need to open more terminals and repeat the method. 