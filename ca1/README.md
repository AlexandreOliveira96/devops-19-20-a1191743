DEVOPS - SWITCH 2019/2020 - B106 - Alexandre Oliveira - 1191743
# Class Assignment 1 
Este tutorial tem como objetivo apresentar uma simples linha de orientação para controlo de versões em Git.



## Análise e Implementação
### Requisitos
Irei assumir que, ao chegar a este ponto, o Git já terá sido instalado, assim como criado um repositório privado no Bitbucket (por exemplo). Se não for o caso, pode clicar em baixo.

- Instalar Git. [Git download](https://git-scm.com/downloads)
- Criar um repositório pessoal. [Bitbucket](bitbucket.org)
- Utilizar uma linha de comandos.


### Clonar o Repositório
Este tutorial irá ser acompanhado e partir com os dados obtidos em [Tutorial React.js and Spring Data REST application](https://github.com/spring-guides/tut-react-and-spring-data-rest).

Para começar será preciso navegar (na CMD) até ao directório onde queremos colocar o nosso repositório.
Para isto, siga os comandos descritos.

```sh
$ cd /localização/do/repo
```

Agora vamos clonar todos os ficheiros para este nosso repositório.
```sh
$ git clone [url]
```
Este **clone** corresponde, no fundo, a uma combinação de 4 ou mais comandos sucessivos:

`$ git init `  
`$ git remote add `  
`$ git fetch `  
`$ git checkout `  

### Enviar dados para o Master
Master é a linha principal de um repositório, ou seja, o branch pai de onde poderão sair novos branches. O primeiro push para o repositório vai marcar a primeira e atual versão estável.

```sh
$ git push -u origin --all
```
### Tags
Tags são algo que nos permite aceder facilmente a um estado de implementação de um projecto. À medida que as alterações sejam feitas no repositório, deveremos marcar as versões.
Assim vamos criar uma tag nova, com o nome "v1.2.0" (por exemplo).
```sh
$ git tag v1.2.0
```
Depois de criar a tag iremos associá-la a um push.
```sh
$ git push origin <tagname>
```
Desta forma iremos aceder às tags usadas no projecto.
```sh
$ git tag
```

### Criar Branches
Como já disse, o master é o branch principal, onde iremos colocar as versões estáveis. Como várias pessoas poderão estar a utilizar e editar o mesmo projeto, é importante que as pessoas possam criar uma versão paralela. 
Aqui podemos editar o ficheiro, sem correr o risco de complicações ou sobreposições de informação. Vamos então criar um novo branch.

```sh
$ git branch new-branch-name
```

Para navegarmos até ao novo branch. 
```sh
$ git checkout branch-name
```

### Alterar o projeto
Nesta fase o utilizador poderá alterar o seu código, ficheiros ou o que quiser, enquanto está no branch.
Deverá testar tudo, para evitar conflitos.

### Submeter para o repositório 
O seguinte comando irá adicionar ao repositório todas as diferenças entre as versões.

```sh
$ git add .
```
Deveremos realizar commit das alterações, com uma mensagem ou um issue atribuído.

```sh
$ git commit -m "your message"
```

Fazer push dos branches locais para o repositório.

```sh
$ git push -u origin <branch>
```

### Nota: projectos colectivos
Quando estamos a trabalhar com outras pessoas, teremos que ter atenção ao momento em que voltamos a unir o nosso branch com o master.
Devemos sempre realizar um pull antes do nosso push. Isto para podermos fazer o nosso push na versão mais atualizada do projeto, tentando ao máximo evitar
conflitos e sobreposições.
```sh
$ git pull <remote>
```

### Voltar ao master
O comando checkout permite-nos navegar entre vários branches, usando o comando seguinte para voltar ao master.
```sh
$ git checkout master
```
Para integrarmos as nossas novas alterações com o projeto, iremos realizar um merge com o master.
Usando um IDE, podemos corrigir facilmente quaisquer conflito que seja encontrado. 
Usando a CMD, podemos instalar uma interface visual que nos ajude a perceber o que poderá estar sobreposto e escolher a melhor versão.
```sh
$ git merge <branch>
```
Podemos colocar uma nova tag e continuar a trabalhar.
```sh
$ git tag v1.3.0
$ git push origin v1.3.0
```
-----------------
#Alternativa
Para alternativa ao Git, escolhi usar o Mercurial para controlo de versões e o Repositório HelixTeamHub. 
O readme para esse repositório está alocado no seguinte link, ao qual dei permissão aos professores.
[Link para o repo HelixTeamHub](https://1191743isepipppt@helixteamhub.cloud/big-shovel-3695/projects/devops-19-20-a1191743/repositories/mercurial/devops-19-20-a1191743).

Na minha opinião, o controlo de versões em Git é mais fácil e direta, sendo que no caso do Mercurial, a segurança é uma questão privilegiada, pedindo uma password para cada push.
Por ser mais usado, é mais acessível ajuda para comandos Git em vez do Merucurial, sendo no entanto comandos bastante parecidos e partindo dos mesmos pressupostos.


## Implementação da Alternativa em Mercurial e HelixHubTeam
(Esta segunda parte pode também ser consultada no repositório Mercurial, ao qual deixei o link em cima)


Os passos serão basicamente os mesmos, mas com uma sintaxe diferente, daí que não vá explorar tanto o que cada um faz.

Vamos continuar a seguir o [Tutorial React.js and Spring Data REST application](https://github.com/spring-guides/tut-react-and-spring-data-rest).

Navegar até o local onde queremos colocar o nosso repositório.
```sh
$ cd /local/do/repo
```

Clonar todos os ficheiros para o repositório.
```sh
$ hg clone [url]
```
Inicializar o diretorio como um repositorio Mercurial.
```sh
$ hg init
```
Adicionar o repositorio ao ficheiro Mercurial.ini .
```
[paths]
default = <url>
default-push = <url>
```

Adicionar tudo ao repo.
```sh
$ hg add .
```
Commit dos ficheiros, exemplo primeiro commit.
```sh
$ hg commit -m "Initial commit"
```
Criar uma tag para o próximo push.
```sh
$ hg tag <tag-name>
```
Push das alterações para o repo.
```sh
$ hg push <url-to-remote-repository>
```
Criar um novo branch para realizar alterações.
```sh
$ hg branch email-field
```

Alterar o ficheiro

Testar as novas features

Enviar as alterações para o repo com um commit.

```sh
$ hg commit -m "message"
```
Enviar as alterações para o repo.
```sh
$ hg push --new-branch
```

Voltar para o ramo principal, ao contrario do Git, aqui chama-se Default.
```sh
$ hg uptade default
```
Juntar as novas alterações ao projecto
```sh
$ hg merge <branch-name>
```
Commit dos ficheiros.
```sh
$ hg commit -m "message"
```
Marcar com uma tag a versão do projeto.
```sh
$ hg tag <tag-name>
```
Voltar a enviar as alterações com a nova tag.
```sh
$ hg push
```