package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    Employee alex = new Employee("Alex", "Oliveira", "estudante",
            "aluno pos graduação", "email@isep.pt");

    @Test
    void getJobTitle() {

        assertEquals("aluno pos graduação", alex.getJobTitle());
    }

    @Test
    void setJobTitle() {
        alex.setJobTitle("quarentena");
        assertEquals("quarentena", alex.getJobTitle());
    }

    @Test
    void testEquals() {
        Employee alex2 = new Employee("A", "B",
                "C", "D", "email@email.pt");
        assertNotEquals(alex, alex2);
    }

    @Test
    void testEqualsSame() {
        Employee alex2 = new Employee(alex.getFirstName(), alex.getLastName(), alex
                .getDescription(), alex.getJobTitle(), alex.getEmail());
        assertEquals(alex, alex2);
    }

    @Test
    void testToString() {
        String actual = alex.toString();
        String expected = "Employee: Alex, Oliveira, estudante, aluno pos graduação, email@isep.pt";

        assertEquals(actual, expected);
    }

    @Test
    void getId() {
        assertNull(alex.getId());
    }

    @Test
    void setId() {
        alex.setId(2L);
        assertEquals(2L, alex.getId());
    }

    @Test
    void getFirstName() {
        assertEquals("Alex", alex.getFirstName());
    }

    @Test
    void setFirstName() {
        alex.setFirstName("Joao");
        assertEquals("Joao", alex.getFirstName());
    }

    @Test
    void getLastName() {
        assertEquals("Oliveira", alex.getLastName());
    }

    @Test
    void setLastName() {
        alex.setLastName("Joao");
        assertEquals("Joao", alex.getLastName());
    }

    @Test
    void getDescription() {
        assertEquals("estudante", alex.getDescription());
    }

    @Test
    void setDescription() {
        alex.setDescription("quarentena");
        assertEquals("quarentena", alex.getDescription());
    }

    @Test
    void getEmail() {
        assertEquals("email@isep.pt", alex.getEmail());
    }

    @Test
    void setEmail() {
        alex.setEmail("alex@isep.pt");
        assertEquals("alex@isep.pt", alex.getEmail());

    }

    @Test
    void constructor() {
        try {
            new Employee("a", null, ",", ",", "");
        } catch (IllegalArgumentException e) {
            assertEquals("Fields can't be empty.", e.getMessage());
        }

    }

    @Test
    void constructorEmails() {
        try {
            new Employee("a", "null", "a", "a", "email.pt");
        } catch (IllegalArgumentException e) {
            assertEquals("Email is not valid.", e.getMessage());
        }

    }

}
