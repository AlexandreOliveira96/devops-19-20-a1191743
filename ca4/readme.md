# Class Assignment 4 

### Prerequisites
To perform this tutorial will need to install Docker on your computer.
Since I didn't have Windows Pro, my computer doesn't have Hyper-V, so I had to install
[Docker Toolbox](https://docs.docker.com/toolbox/toolbox_install_windows/) instead.
You will also need to create your [Docker-Hub](https://hub.docker.com) account and create your personal repository.
 
### Implementation
 
 To get started we just need to clone the professor's [repository](https://bitbucket.org/atb/docker-compose-spring-tut-demo/src/master/)
 into our new directory called Ca4. We will then update the `Dockerfile` from 
 our web machine so that it clones our repository, as we did with vagrant in Ca3-parte2.
 
 ```
 RUN git clone https://AlexandreOliveira96@bitbucket.org/AlexandreOliveira96/devops-19-20-a1191743.git
 ```
 
I also changed my work directory so that if fits perfectly into my folder with the gradle files, 
 since we will need to build this project.
 
```
WORKDIR /tmp/build/devops-19-20-a1191743/ca3/Parte2/gradle
```

To make it work I also gave permission to the dockerfile to perform `gradle` tasks. 

```
 RUN chmod +x gradlew
```


After this we should cd into our dirctory with the docker-compose.yml file and build it.

```sh
$ docker-compose build
```

This will take a while but after it performs every task succesfully, we can advance into our
next goal, to get it to work on the web.

```sh
$ docker-compose up
```

### Checking the browser

Since I used the same repository as before, everything should be prepared to be handled in the 
frontend. So, let's open up the browser with:

```
http://192.168.99.101:8080/demo-0.0.1-SNAPSHOT/
```

If everything worked out, you should be able to view the application with some starting data.

 ![](data/Capturar.PNG)


We can also open the browser and check our H2 console, using the following url.

```
http://192.168.99.101:8080/demo-0.0.1-SNAPSHOT/h2-console
```

By entering the string `jdbc:h2:tcp://192.168.33.11:9092/./jpadb` we can connect
with the database and do some SQL tasks. Such as inserting
new employees, update or delete data and view the full table.
After playing with the database for a while, we cant continue our project.

### Publishing the images

Next we are going to login into our account of Docker-Hub using the command.

```sh
$ docker login
```

It should then ask you for your DockerID and password. If the login is successful we can continue.
To check the name of the images you want to publish you can just perform:

```sh
$ docker images
```

You should be able to see your web and your db images. 
Now we will tag the web image using:

```sh
$ docker tag ca4_web alexandreoliveira96/devops-10-20-a1191743:web_spring_boot_application
```

and the db image:

```sh
$ docker tag ca4_db alexandreoliveira96/devops-10-20-a1191743:db_spring_boot_application
```

After this, we can push them into our repository:

```sh
$ docker push alexandreoliveira96/devops-19-20-a1191743
```

![](data/Capturar1.PNG)

This will allow anyone to use our new created images, using a simple docker pull, like this:

```sh
$ docker pull alexandreoliveira96/devops-19-20-a1191743:web_spring_boot_application
$ docker pull alexandreoliveira96/devops-19-20-a1191743:db_spring_boot_application
```

### Copying the database

Now we will copy the database into our local repository, by using the exec to run 
a shell in the container and copying the database file to the volume.

```sh
$ docker-compose exec db /bin/bash
```

and then

```
$ cp ./jpadb.mv.db /usr/src/data
```

We can make sure everything worked out properly by listing the files in data directory

```
$ ls /usr/src/data –al
```

## Docker Analysis

Docker is a container technology, that allows you to operate system virtualization 
to run an application. It let's you package your application code, it's dependencies and 
configurations. Then you can build and run this image quickly everywhere. This
will help you to understand if your product will work on every machine.
By using docker I found it extremely useful and easy to use and understand. 
I only used Docker Toolbox and still a lot of options were performed in a proper way.

### Alternative Analysis

A possible alternative could be, as suggested, Kubernetes. I found out that when 
a number of container instances is required, if a container crashes, a new one is automatically created.
By reading a bit of Kubernetes I learned that it can be used to complement Docker, not
just like an alternative. Docker helps you to develop, deploy and 
so iterate faster on your product whereas Kubernetes is the solution to run them safely in production.