##Devops - Gradle Build Tools CA2

### Parte 1

#####Requisitos: 
`Gradle 6.2.2` `Java JDK 8` `Apache Log4J 2`

Fazer download do [repositório](https://bitbucket.org/luisnogueira/gradle_basic_demo/src/master/ ) disponibilizado 
e colocá-lo num novo diretório `//devops-19-20-a1191743/ca2/parte1`

<br>

####Testar a aplicação:
Se a versão que tivermos do Gradle não for a 6.2.2, precisaremos de
realizar o comando seguinte, para podermos correr a versão disponibilizada. 

```sh
gradlew wrapper  --gradle-version=6.3 --distribution-type=bin 
```

Para corrermos e testarmos a aplicação precisamos de abrir uma linha de comandos.
Primeiro é preciso fazer um ficheiro .jar com a aplicação:
```sh
./gradlew build
```
Depois, vamos até ao diretório onde está localizada a aplicação e fazemos o 
comando seguinte para termos a aplicação a correr em rede. Vamos substituir o 
'server port' pelo que está descrito na aplicação, 59001.
```sh
java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp <server port>
```
Por fim, podemos abrir vários terminais para os clientes que irão usar este chat
, usando o comando:
```sh
./gradlew runClient
```
Temos agora um chat, em várias janelas, a comunicar entre si.
 
 <br>


####Adicionar Tarefas:

Abrindo um IDE à escolha, IntelliJ no meu caso, vamos procurar o ficheiro build.gradle.

Aqui vamos adicionar nova task:
```groovy
 runServer()
```

Depois podemos criar um novo teste(ver slides), na classe AppTest num novo diretório
 `/src/test/java/main_demo` e adicionamos a dependência para o mesmo.
Ainda no ficheiro build.gradle procuramos as dependências e acrescentamos:

```groovy
testImplementation 'junit:junit:4.12'
```
Criamos em seguida a task "test":
```groovy
 test { 
    useJUnit()  
 }
```
####Mover ficheiros:
Vamos agora criar uma nova task para mover ficheiros do diretório Src para 
um novo diretório Backup, que vamos criar. No ficheiro build.gradle escrevemos
 o seguinte:
```groovy
task copyToBackup(type: Copy) {
    from 'src'
    into 'backup'
}
```
Depois criamos uma task para arquivar a pasta source num ficheiro .zip
com o seguinte comando:

```groovy
task zip(type: Zip) {
    from 'src'
    archiveName 'project.zip'
}
```

Neste momento cumprimos todas as tarefas, podemos enviar todas as alterações 
para o nosso repositório com a tag `ca2-parte1`.


<br>

###Parte 2


Para começar a segunda parte vamos criar um novo branch no nosso repositório
com o nome `tut-basic-gradle` onde vamos fazer as próximas alterações.

####Gerar um projecto Gradle
 
Vamos a [start.spring.io](start.spring.io) e geramos um ficheiro zip com o 
projeto em Gradle, com as depencias necessárias para o nosso trabalho:

`Rest Repositories` `Thymeleaf` `JPA` `H2`

Depois de gerado, iremos extrair o ficheiro .zip gerado e colocá-lo
num novo diretorio `//devops-19-20-a1191743/ca2/parte2`

Para vermos as task disponíveis usamos o comando:
```sh
./gradlew tasks 
```


Como vamos querer usar o source do tutorial, vamos apagar a pasta source que recebemos do demo.
Colocamos neste espaço o src do Basic (tutorial) assim como o`webpack.config.js` e o `package.gson` 
para a root da aplicação.

Apagamos o diretório `src/main/resources/static/built/` porque vamos querer
 queremos que a aplicação seja responsável pela sua criação.

Vamos correr a aplicação na web, com o comando:
```sh
./gradlew bootRun
```
Ao abrir o browser não vemos nada porque que o gradle ainda
 não tem o plugin para lidar com o frontend.
 
<br>

####Criar o frontend
 
Para isto, precisamos de um plugin. Vamos ao build.gradle, 
na zona dos plugins e colamos o seguinte:
```groovy 
id "org.siouan.frontend" version "1.4.1"
```

Ainda dentro do build.gradle, colamos o código seguinte 
para configurar o frontend
```groovy
frontend {
     nodeVersion = "12.13.1"
     assembleScript = "run webpack"
}
```
Atualizamos também os scripts do `package.json` para executar o webpack,
colando o código.
```
 "watch": "webpack --watch -d" 
```

Voltamos a fazer um build para compilar o projeto, gerando o 
código para o frontend.
```sh
 ./gradlew build
```

Depois de compilado podemos voltar a correr a aplicação:
```sh
 ./gradlew bootRun
```

Desta vez já conseguimos ver, no localhost:8080, o esperado pela aplicação.

<br>

####Mais tarefas
De seguida vamos criar uma task para passar o ficheiro Jar gerado, 
para um novo diretório dist, criado na root da aplicação.
```groovy
task copyJarFileIntoDist {
    copy {
        from 'build/libs/'
        into 'dist'
    }
}
```

Depois, criar uma nova task que apague os ficheiros gerados pelo webpack 
que deverão estar em `//static/built`.
```groovy
task deleteWebpackFiles(type: Delete) {
	delete 'src/main/resources/static/built'
}
```

Como queremos que isto aconteça sempre antes da task Clean, 
vamos adicionar ordenar à task clean que dependa do acontecimento da nova 
task criada, da seguinte forma: 
```groovy
clean.dependsOn deleteWebpackFiles
```

Depois de testar todas as novas features da aplicação, podemos voltar a fazer merge com o branch master.

<br>

##Análise do Gradle e de uma alternativa

Sobre o Gradle, devo dizer que é uma ferramenta bastante útil e simples de usar,
com o seu fórum conseguimos, com uma breve pesquisa, perceber o que fazer para
qualquer tarefa que precisemos. A sua codificação, em Groovy, é muito prática,
não dependendo de grandes conhecimentos, por ser extremamente linear e direta.
Apesar de não ser uma ferramenta muito complexa, adapta-se muito bem a diversos
plugins, ganhando muitas funcionalidades.

Para comparação investiguei a ferramenta Apache Ant. Como foi o primeiro sistema
de compilação, foi muito útil, mas apresenta algumas limitações, como o facto de
caber ao utilizador a criação de todas as dependências, manualmente, o que leva a
ficheiros XML muito grandes e difíceis de manter. Também, não é fácil, para alguém
sem experiência em Ant, ficar familiriazado com a sua linguagem e formato.




